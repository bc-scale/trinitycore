#!/usr/bin/env sh

# MIT License
# Copyright (c) 2017-2022 Nicola Worthington <nicolaw@tfb.net>
# https://github.com/neechbear/trinitycore

set -e

looksLikeGitRef() {
  echo "$1" | grep -Eq '^[a-f0-9]{40}$'
}

isDockerEnv() {
  [ -e /.dockerenv ]
}

isTCContainer() {
  isDockerEnv \
  && [ -e /.git-rev ] \
  && [ -e /.git-rev-short ] \
  && [ -e /.tdb-full-url ]
}

getImageTCLabel() {
  local image="$1"; shift
  local key=""
  for key in "$@" ; do
    local label="org.opencontainers.image.trinitycore.$key"
    local filter="{{ index .Config.Labels \"$label\" }}"
    docker inspect -f "$filter" "$image"
  done
}

printHelp() {
  if isTCContainer ; then
    echo "Usage: getsql [-h|--help] [LATEST|commit-ref]"
  else
    echo "Usage: getsql [-h|--help] <LATEST|commit-ref|image>"
  fi

  echo "Downloads SQL files from the TrinityCore Git repository."
  echo ""
  echo "Download the most recent version available:"
  echo ""
  echo "  $ getsql LATEST"
  echo ""
  echo "Download an explcit version by Git commit reference:"
  echo ""
  echo "  $ getsql fd7f48545ce76740065860c0963194c2a9e3e0e2"
  echo ""

  if isTCContainer ; then
    echo "Download the version required by this container image:"
    echo ""
    echo "  $ getsql"
    echo ""
  else
    echo "Download the labelled and required by a specific container image:"
    echo ""
    echo "  $ getsql nicolaw/trinitycore:3.3.5-slim"
    echo ""
  fi
}

downloadSqlFromGit() {
  local remote="$1"
  local ref="$2"
  local dir="."
  local gitdir="$dir/.git-getsql.tmp"
  local checkoutRef="$ref"
  if ! looksLikeGitRef "$checkoutRef" ; then
    checkoutRef="origin/$checkoutRef"
  fi

  set -x
  git --git-dir "$gitdir" init -q "$dir"
  if git --git-dir "$gitdir" -C "$dir" remote show origin 2>/dev/null ; then
    git --git-dir "$gitdir" -C "$dir" remote set-url origin "$remote"
  else
    git --git-dir "$gitdir" -C "$dir" remote add origin "$remote"
  fi
  git --git-dir "$gitdir" -C "$dir" fetch -q --depth 1 origin "$ref"
  git --git-dir "$gitdir" --work-tree "$dir" -C "$dir" checkout "$checkoutRef" -- 'sql/*'
  rm -rf -- "$gitdir"
}

main() {
  local arg=""
  for arg in "$@" ; do
    if [ "$arg" = "-h" ] || [ "$arg" = "--help" ] ; then
      printHelp >&2
      exit 0
    fi
  done

  local version="$1"
  if ! isTCContainer && [ -z "$version" ] ; then
    printHelp >&2
    exit 1
  fi

  if [ -z "$version" ] ; then
    downloadSqlFromGit \
        "https://github.com/TrinityCore/TrinityCore.git" \
        "$(cat /.git-rev)"

  else
    case "$version" in
      LATEST|latest|3.3.5|335|3.3.5a|335a)
        downloadSqlFromGit \
          "https://github.com/TrinityCore/TrinityCore.git" \
          "3.3.5"
        ;;

      master)
        downloadSqlFromGit \
          "https://github.com/TrinityCore/TrinityCore.git" \
          "master"
        ;;

      *)
        if looksLikeGitRef "$version" ; then
          downloadSqlFromGit \
            "https://github.com/TrinityCore/TrinityCore.git" \
            "$version"

        else
          downloadSqlFromGit \
            "$(getImageTCLabel "$version" source)" \
            "$(getImageTCLabel "$version" revision)"
        fi
        ;;
    esac
  fi
}

main "$@"
